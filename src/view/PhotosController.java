package view;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.PhotoSearcher;
import model.Tag;
import model.TagType;
import model.User;
import model.UsersData;
import model.Value;

public class PhotosController{
	
	
	@FXML ListView<String> userList;
	@FXML ListView<Album> albumList;
	@FXML ListView<Photo> photoList;
	@FXML ListView<Photo> searchPhotoList;
	@FXML ListView<Tag> tagList;
	@FXML ListView<TagType> typeList;
	@FXML private TextField loginUserTxt;
	@FXML private TextField addUserTxt;
	@FXML private TextField newAlbumTxt;
	@FXML private TextField changeAlbumTxt;
	@FXML private TextField dateTxt;
	@FXML private TextField tagTxt;
	@FXML private TextField tagTypeTxt;
	@FXML private TextField tagValueTxt;
	@FXML private TextField captionTxt;
	@FXML private ImageView imageDisplay;
	@FXML private ImageView photoView;
	@FXML private Label photoDetails;
	private Stage currentStage;
	private List<String> users;
	private ObservableList<String> obsList;
	private ObservableList<Tag> tagObsList;
	private ObservableList<TagType> typeObsList;
	private ObservableList<Album> albumObsList;
	private ObservableList<Photo> photoObsList;
	private User currentUser;
	private Album selAlbum;
	private Photo selPhoto;
	private List<Photo> searchedPhotos;
	private int slideshowIndex = 0;
	private int moveOrCopy = 0; //movePhotoButton was pressed if 0 
								//copyPhotoButton was pressed if 1
	
	/**
	 * Sets the displaying screen's stage to mainStage
	 * 
	 * @param mainStage
	 * @param username
	 * @throws IOException
	 */
	public void start(Stage mainStage) throws IOException {
		currentStage = mainStage;
		if(photoView != null) {
			Image i = new Image(selPhoto.getFile().toURI().toString());
			photoView.setImage(i);}
		if(photoDetails != null) {
			photoDetails.setText("Caption: " + selPhoto.getCaption() + "\nDate: " + selPhoto.getStringDate());
		}
		users = UsersData.getUserList();
		if(users.size() != 0) {
			obsList = FXCollections.observableArrayList(users);
			if(userList != null) {
				userList.setItems(obsList);
				userList.getSelectionModel().select(0);
				obsList = FXCollections.observableArrayList(users);
				userList.setItems(obsList);
			}
		}
		if(albumList != null) {
			albumObsList = FXCollections.observableArrayList(currentUser.getAlbums());
			albumList.setItems(albumObsList);
			albumList.getSelectionModel().select(0);
		}
		if(tagList != null) {
			tagObsList = FXCollections.observableArrayList(selPhoto.getTags());
			tagList.setItems(tagObsList);
			tagList.getSelectionModel().select(0);
		}
		if(typeList != null) {
			typeObsList = FXCollections.observableArrayList(currentUser.getTagTypes());
			typeList.setItems(typeObsList);
			typeList.getSelectionModel().select(0);
		}
		if(photoList != null) {
			Album album = currentUser.getAlbum(selAlbum);
			List<Photo> photos = album.getPhotos();
			photoObsList = FXCollections.observableArrayList(photos);
			photoList.setItems(photoObsList);
			photoList.setCellFactory(param -> new ListCell<Photo>() {
				public ImageView iView = new ImageView();
				@Override
				public void updateItem(Photo photo, boolean empty) {
					super.updateItem(photo, empty);
					iView.setFitWidth(100);
					iView.setPreserveRatio(true);
					if(empty) {
						setText(null);
						setGraphic(null);
					}
					else {
						Image i = new Image(photo.getFile().toURI().toString());
						iView.setImage(i);
						if(photo.getCaption() != "") {setText(photo.getCaption());}
						setGraphic(iView);
					}
				}
			});
			photoList.getSelectionModel().select(0);
		}
		if(searchPhotoList != null) {
			if(searchedPhotos != null && searchedPhotos.size() > 0) {
				photoObsList = FXCollections.observableArrayList(searchedPhotos);
				searchPhotoList.setItems(photoObsList);
				searchPhotoList.setCellFactory(param -> new ListCell<Photo>() {
					public ImageView iView = new ImageView();
					@Override
					public void updateItem(Photo photo, boolean empty) {
						super.updateItem(photo, empty);
						iView.setFitWidth(100);
						iView.setPreserveRatio(true);
						if(empty) {
							setText(null);
							setGraphic(null);
						}
						else {
							Image i = new Image(photo.getFile().toURI().toString());
							iView.setImage(i);
							if(photo.getCaption() != "") {setText(photo.getCaption());}
							setGraphic(iView);
						}
					}
				});
			}
		}
	}
	
	/**
	 * In the slideshow scene, the scene displays the next photo in the selected album
	 * 
	 * @throws IOException
	 */
	public void nextPhoto() throws IOException {
		List<Photo> slidePhotos = selAlbum.getPhotos();
		if(slideshowIndex + 1 < slidePhotos.size()) {
			slideshowIndex = slideshowIndex + 1;}
		selPhoto = slidePhotos.get(slideshowIndex);
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/Slideshow_Scene.fxml"));
		Pane root = loader.load();
		PhotosController ctrl = loader.getController();
		ctrl.currentUser = this.currentUser;
		ctrl.selAlbum = this.selAlbum;
		ctrl.selPhoto = this.selPhoto;
		ctrl.searchedPhotos = this.searchedPhotos;
		ctrl.slideshowIndex = this.slideshowIndex;
		ctrl.moveOrCopy = this.moveOrCopy;
		ctrl.start(currentStage);
		Scene scene = new Scene(root);
		currentStage.setScene(scene);
		currentStage.setResizable(false);
		currentStage.show();
	}
	
	/**
	 * In the slideshow scene, the scene displays the previous photo in the selected album
	 * 
	 * @throws IOException
	 */
	public void prevPhoto() throws IOException{
		if(slideshowIndex - 1 >= 0) {
			slideshowIndex = slideshowIndex - 1;}
		List<Photo> slidePhotos = selAlbum.getPhotos();
		selPhoto = slidePhotos.get(slideshowIndex);
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/Slideshow_Scene.fxml"));
		Pane root = loader.load();
		PhotosController ctrl = loader.getController();
		ctrl.currentUser = this.currentUser;
		ctrl.selAlbum = this.selAlbum;
		ctrl.selPhoto = this.selPhoto;
		ctrl.searchedPhotos = this.searchedPhotos;
		ctrl.slideshowIndex = this.slideshowIndex;
		ctrl.start(currentStage);
		Scene scene = new Scene(root);
		currentStage.setScene(scene);
		currentStage.setResizable(false);
		currentStage.show();
	}
	
	/**
	 * Adds a username to the user list (which is viewable to admin)
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void usernameEnter() throws IOException, ClassNotFoundException{
		String username = loginUserTxt.getText();
		if(username != "") {
			if(username.equals("admin")) {
				toAdmin();
			}
			else {
				User user = new User(username);
				currentUser = UsersData.getUser(user);
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getResource("/view/Non-admin.fxml"));
				Pane root = loader.load();
				PhotosController ctrl = loader.getController();
				ctrl.currentUser = this.currentUser;
				ctrl.start(currentStage);
				Scene scene = new Scene(root);
				currentStage.setScene(scene);
				currentStage.setResizable(false);
				currentStage.show();
			}
		}
		else {
			Alert alert = new Alert(AlertType.ERROR, "Username cannot be empty");
			alert.initOwner(currentStage);
			alert.showAndWait();
		}
		
	}
	
	/**
	 * Switches scene to display the scene with the name sceneName
	 * 
	 * @param sceneName
	 * @throws IOException
	 */
	public void changeScene(String sceneName) throws IOException{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/" + sceneName));
		Pane root = loader.load();
		PhotosController ctrl = loader.getController();
		ctrl.currentUser = this.currentUser;
		ctrl.selAlbum = this.selAlbum;
		ctrl.selPhoto = this.selPhoto;
		ctrl.searchedPhotos = this.searchedPhotos;
		ctrl.slideshowIndex = this.slideshowIndex;
		ctrl.moveOrCopy = this.moveOrCopy;
		ctrl.start(currentStage);
		Scene scene = new Scene(root);
		currentStage.setScene(scene);
		currentStage.setResizable(false);
		currentStage.show();
	}
	
	/**
	 * Switches to Admin scene
	 * 
	 * @throws IOException
	 */
	public void toAdmin() throws IOException {
		changeScene("Admin.fxml");
	}
	
	/**
	 * Switches to Non-admin scene
	 * 
	 * @throws IOException
	 */
	public void toNonAdmin() throws IOException{
		searchedPhotos = null;
		changeScene("Non-Admin.fxml");
	}
	
	/**
	 * Switches to Add_User_Scene scene
	 * 
	 * @throws IOException
	 */
	public void toAddUserScene() throws IOException {
		changeScene("Add_User_Scene.fxml");
	}
	
	/**
	 * Switches to Rename_Album_Scene
	 * 
	 * @throws IOException
	 */
	public void toRenameAlbumScene() throws IOException{
		if(currentUser.getAlbums().size() != 0) {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/Rename_Album_Scene.fxml"));
			Pane root = loader.load();
			PhotosController ctrl = loader.getController();
			ctrl.currentUser = this.currentUser;
			int albumIndex = albumList.getSelectionModel().getSelectedIndex();
			Album album = currentUser.getAlbums().get(albumIndex);
			selAlbum = album;
			ctrl.selAlbum = this.selAlbum;
			ctrl.selPhoto = this.selPhoto;
			ctrl.slideshowIndex = this.slideshowIndex;
			ctrl.moveOrCopy = this.moveOrCopy;
			ctrl.start(currentStage);
			ctrl.changeAlbumTxt.setText(album.getName());
			Scene scene = new Scene(root);
			currentStage.setScene(scene);
			currentStage.setResizable(false);
			currentStage.show();}
	}
	
	/**
	 * Changes the caption of the selected photo
	 * 
	 * @throws IOException
	 */
	public void editCaption() throws IOException {
		String caption = captionTxt.getText();
		selPhoto.setCaption(caption);
		toAlbumDisplay();
	}
	
	public void renameAlbum() throws IOException {
		if(currentUser.getAlbums().size() != 0) {
			String newName = changeAlbumTxt.getText();
			if(newName != "") {
				currentUser.renameAlbum(selAlbum, newName);}
			else{
				Alert alert = new Alert(AlertType.ERROR, "Album name cannot be empty");
				alert.initOwner(currentStage);
				alert.showAndWait();
			}
		}
		toNonAdmin();
	}
	
	public void toCreateAlbumScene() throws IOException{
		if(slideshowIndex == 0 && selAlbum != null) {
			List<Photo> slidePhotos = selAlbum.getPhotos();
			selPhoto = slidePhotos.get(slideshowIndex);
		}
		changeScene("Create_Album_Scene.fxml");
	}
	
	public void toSearchResult() throws IOException{
		searchedPhotos = new ArrayList<>();
		changeScene("Search_Result.fxml");
	}
	
	public void toAlbumDisplay() throws IOException{
		if(currentUser.getAlbums().size() != 0) {
			if(albumList != null) {
				int albumIndex = albumList.getSelectionModel().getSelectedIndex();
				Album album = currentUser.getAlbums().get(albumIndex);
				selAlbum = album;}
			changeScene("Album_Display.fxml");}
	}
	
	public void toPhotoDisplay() throws IOException{
		if(selPhoto != null) {
			selPhoto = photoList.getSelectionModel().getSelectedItem();
			changeScene("Photo_Display.fxml");}
	}
	
	public void toSlideshowScene() throws IOException{
		if(selAlbum.getPhotos().size() > 0) {
			List<Photo> slidePhotos = selAlbum.getPhotos();
			selPhoto = slidePhotos.get(0);
			slideshowIndex = 0;
			changeScene("Slideshow_Scene.fxml");}
	}
	
	public void toAlbumList() throws IOException{
		if(photoList != null) {
			selPhoto = photoList.getSelectionModel().getSelectedItem();}
		changeScene("Album_List.fxml");
	}
	
	public void toEditingPhoto() throws IOException{
		if(photoList != null) {
			selPhoto = photoList.getSelectionModel().getSelectedItem();}
		changeScene("Editing_Photo.fxml");
	}
	
	public void toTagScene() throws IOException {
		changeScene("Tag_Scene.fxml");
	}
	
	public void toCaptionScene() throws IOException{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/Caption_Scene.fxml"));
		Pane root = loader.load();
		PhotosController ctrl = loader.getController();
		ctrl.currentUser = this.currentUser;
		ctrl.selAlbum = this.selAlbum;
		ctrl.selPhoto = this.selPhoto;
		ctrl.slideshowIndex = this.slideshowIndex;
		ctrl.moveOrCopy = this.moveOrCopy;
		ctrl.start(currentStage);
		ctrl.captionTxt.setText(selPhoto.getCaption());
		Scene scene = new Scene(root);
		currentStage.setScene(scene);
		currentStage.setResizable(false);
		currentStage.show();
	}
	
	public void deleteTag() {
		Tag delTag = tagList.getSelectionModel().getSelectedItem();
		selPhoto.removeTag(delTag);
		tagObsList = FXCollections.observableArrayList(selPhoto.getTags());
		if(tagList != null) {
			tagList.setItems(tagObsList);
			tagList.getSelectionModel().select(0);
		}
	}
	
	public void deletePhoto() throws IOException {
		Photo delPhoto = photoList.getSelectionModel().getSelectedItem();
		selAlbum.removePhoto(delPhoto);
		photoObsList = FXCollections.observableArrayList(selAlbum.getPhotos());
		if(photoList != null) {
			photoList.setItems(photoObsList);
			photoList.getSelectionModel().select(0);
		}
	}
	
	public void deleteUser(){
		int userIndex = userList.getSelectionModel().getSelectedIndex();
		String delUser = users.get(userIndex);
		try {
			UsersData.deleteUser(delUser);
		} catch (IOException e) {
			e.printStackTrace();
		}
		users.remove(userIndex);
		obsList = FXCollections.observableArrayList(users);
		if(userList != null) {
			userList.setItems(obsList);
			userList.getSelectionModel().select(0);
		}
	}
	
	public void deleteAlbum(){
		if(currentUser.getAlbums().size() != 0) {
			int albumIndex = albumList.getSelectionModel().getSelectedIndex();
			Album delAlbum = currentUser.getAlbums().get(albumIndex);
			try {
				currentUser.deleteAlbum(delAlbum);
			} catch (IOException e) {
				e.printStackTrace();
			}
			albumObsList = FXCollections.observableArrayList(currentUser.getAlbums());
			if(albumList != null) {
				albumList.setItems(albumObsList);
				albumList.getSelectionModel().select(0);
			}}
	}
	
	public void addUser() throws IOException, ClassNotFoundException {
		String username = addUserTxt.getText();
		if(username != "") {
			User user = new User(username);
			currentUser = UsersData.getUser(user);
			toAdmin();}
		else {
			Alert alert = new Alert(AlertType.ERROR, "Username cannot be empty");
			alert.initOwner(currentStage);
			alert.showAndWait();
		}
	}
	
	public void createFromSearch() throws IOException {
		String albumName = newAlbumTxt.getText();
		if(albumName != "") {
			Album searchAlbum = new Album(albumName, currentUser);
			try {
				if (searchedPhotos.size() > 0) {
					if(currentUser.getAlbum(new Album(albumName, currentUser)) == null) {
						for (Photo photo : searchedPhotos) {
							photo.setAlbum(searchAlbum);
							searchAlbum.addPhoto(photo);
						}
						currentUser.createAlbum(searchAlbum);
					}
					else {
						Alert alert = new Alert(AlertType.ERROR, "Album name already exists");
						alert.initOwner(currentStage);
						alert.showAndWait();
					}
				} else {
					Alert alert = new Alert(AlertType.ERROR, "No Photos to Create Album with");
					alert.initOwner(currentStage);
					alert.showAndWait();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			toNonAdmin();
		}
		else {
			Alert alert = new Alert(AlertType.ERROR, "Album name cannot be empty");
			alert.initOwner(currentStage);
			alert.showAndWait();
		}
	}
	
	public void createAlbum() throws IOException {
		String albumName = newAlbumTxt.getText();
		if(albumName != "") {
			if(searchedPhotos != null) {
				createFromSearch();
			}
			else {
				try {
					if(currentUser.getAlbum(new Album(albumName, currentUser)) == null) {
						currentUser.createAlbum(albumName);}
					else {
						Alert alert2 = new Alert(AlertType.ERROR, "Album name already exists");
						alert2.initOwner(currentStage);
						alert2.showAndWait();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				toNonAdmin();
			}
		}
		else {
			Alert alert = new Alert(AlertType.ERROR, "Album name cannot be empty");
			alert.initOwner(currentStage);
			alert.showAndWait();
		}
	}
	
	public void addPhoto() throws IOException {
		FileChooser fileChooser = new FileChooser();
		String userDirectoryString = System.getProperty("user.home");
		File userDirectory = new File(userDirectoryString);
		if(!userDirectory.canRead()) {
			userDirectory = new File("c:/");
		}
		fileChooser.setInitialDirectory(userDirectory);
		File chosenFile = fileChooser.showOpenDialog(null);
		if(chosenFile != null) {
			List<String> extensions = Photo.getExtensions();
			if(!extensions.contains(Photo.getFileExtension(chosenFile))){
				Alert alert2 = new Alert(AlertType.ERROR, "Must choose BMP, GIF, JPEG, or PNG file");
				alert2.initOwner(currentStage);
				alert2.showAndWait();
			}
			else {
				Album album = currentUser.getAlbum(selAlbum);
				album.addPhoto(new Photo(chosenFile, album));}
			toAlbumDisplay();}
	}
	
	/**
	 * Adds a new tag to the selected photo
	 */
	public void addTag(){
		TagType type = typeList.getSelectionModel().getSelectedItem();
		String tagValue = tagValueTxt.getText();
		Tag tag = new Tag(type.getName(), tagValue);
		if(type.getValue() == Value.SINGLE) {
			for(Tag t : selPhoto.getTags()) {
				if(t.getKey() == tag.getKey()) {
					selPhoto.removeTag(t);
					break;
				}
			}
			selPhoto.addTag(tag);
		}
		else {
			selPhoto.addTag(tag);
		}
		tagObsList = FXCollections.observableArrayList(selPhoto.getTags());
		if(tagList != null) {
			tagList.setItems(tagObsList);
			tagList.getSelectionModel().select(0);
		}
	}
	
	/**
	 * Adds a tag type to photo
	 * @throws IOException
	 */
	public void addTagType() throws IOException {
		String txt = tagTypeTxt.getText();
		TagType tag;
		Alert alert = new Alert(AlertType.CONFIRMATION, "Can this tag type have multiple values?", ButtonType.YES, ButtonType.NO);
		alert.initOwner(currentStage);
		alert.showAndWait();
		if(alert.getResult() == ButtonType.YES) {
			tag = new TagType(txt, Value.MULTIPLE);
		}
		else {
			tag = new TagType(txt, Value.SINGLE);
		}
		currentUser.addTagType(tag);
		typeObsList = FXCollections.observableArrayList(currentUser.getTagTypes());
		if(typeList != null) {
			typeList.setItems(typeObsList);
			typeList.getSelectionModel().select(0);
		}
	}
	
	public void search() throws ParseException {
		String dateRange = dateTxt.getText();
		String tag = tagTxt.getText();
		if(dateRange == "" && tag == "") {
			Alert alert = new Alert(AlertType.ERROR, "Must enter a date or a tag to search for photos");
			alert.initOwner(currentStage);
			alert.showAndWait();
		}
		else {
			if(dateRange != "" && tag != "") {
				Alert alert2 = new Alert(AlertType.ERROR, "Can only search for date range OR tag(s)");
				alert2.initOwner(currentStage);
				alert2.showAndWait();
			}
			if(PhotoSearcher.correctDateFormat(dateRange) == true) {
				PhotoSearcher photoSearcher = new PhotoSearcher(currentUser);
				Date dates[] = PhotoSearcher.stringToDates(dateRange);
				List<Photo> photos = new ArrayList<>();
				if (dates[1] == null) {
					photos = photoSearcher.search(dates[0]);
				} else {
					if (PhotoSearcher.datesCorrectOrder(dates)) {
						photos = photoSearcher.search(dates);
					} else {
						Alert alert = new Alert(AlertType.ERROR, "Dates in Wrong Order");
						alert.initOwner(currentStage);
						alert.showAndWait();
					}
				}
				if (photos.size() > 0) {
					searchedPhotos = photos;
					photoObsList = FXCollections.observableArrayList(photos);
					searchPhotoList.setItems(photoObsList);
					searchPhotoList.setCellFactory(param -> new ListCell<Photo>() {
						public ImageView iView = new ImageView();
						@Override
						public void updateItem(Photo photo, boolean empty) {
							super.updateItem(photo, empty);
							iView.setFitWidth(100);
							iView.setPreserveRatio(true);
							if(empty) {
								setText(null);
								setGraphic(null);
							}
							else {
								Image i = new Image(photo.getFile().toURI().toString());
								iView.setImage(i);
								if(photo.getCaption() != "") {setText(photo.getCaption());}
								setGraphic(iView);
							}
						}
					});
				}
			}
			else {
				Alert alert2 = new Alert(AlertType.ERROR, "Date format should be MM/DD/YYYY-MM/DD/YYYY or MM/DD/YYYY");
				alert2.initOwner(currentStage);
				alert2.showAndWait();
			}
		}
	}
	
	public void movePhoto() throws IOException {
		moveOrCopy = 0;
		toAlbumList();
	}
	
	public void copyPhoto() throws IOException {
		moveOrCopy = 1;
		toAlbumList();
	}
	
	public void changePhotoLocation() throws IOException {
		int albumIndex = albumList.getSelectionModel().getSelectedIndex();
		Album toAlbum = currentUser.getAlbums().get(albumIndex);
		if(moveOrCopy == 0) {
			selAlbum.movePhoto(selPhoto, toAlbum);}
		else{selAlbum.copyPhoto(selPhoto, toAlbum);} 
		toAlbumDisplay();
	}
	
	public void prevScene() throws IOException {
		if(searchedPhotos != null) {
			toSearchResult();}
		else {toNonAdmin();}
	}
	
	public void logout() throws IOException {
		currentUser = null;
		changeScene("Login_Scene.fxml");
	}

}
