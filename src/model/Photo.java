package model;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Photo that is associated with a file and album. Has a caption and tags metadata.
 *
 * @author Kenneth Salanga
 */
public class Photo implements Serializable, Comparable<Photo> {
    /**
     * ID when generating serialized object file.
     */
    static final long serialVersionUID = 4L;

    /**
     * File that is associated with a photo.
     */
    private final File file;

    /**
     * Album that is associated with a photo.
     */
    private Album album;

    /**
     * Caption of photo.
     */
    private String caption;

    /**
     * Tags of photo.
     */
    private final List<Tag> tags;

    /**
     * Calendar object of photo file's last modified date.
     */
    private final Calendar dateLastModified;

    /**
     * Allowed extensions for a photo.
     */
    private static final List<String> extensions = new ArrayList<>(Arrays.asList("bmp", "gif", "jpeg", "png", "jpg"));

    /**
     * Instantiates a new photo with a file and its respective Album.
     *
     * @param file File of photo
     * @param album Album that photo is in
     */
    public Photo(File file, Album album) {
        this.file = file;
        this.album = album;
        this.caption = "";
        this.tags = new ArrayList<>();
        dateLastModified = new GregorianCalendar();
        zeroOutDate();
    }

    /**
     * Instantiates a photo with another photo object.
     * Used for copying purposes if a new photo wants to grab the metadata of an existing photo.
     *
     * @param photo Photo object to copy
     */
    public Photo(Photo photo) {
        this.file = photo.getFile();
        this.album = photo.getAlbum();
        this.caption = photo.getCaption();
        this.tags = photo.getTags();
        dateLastModified = photo.getCalendar();
    }

    /**
     * Gets File of photo.
     *
     * @return file of photo
     */
    public File getFile() {
        return file;
    }

    /**
     * Gets album of photo.
     *
     * @return album of photo
     */
    public Album getAlbum() {
        return album;
    }

    /**
     * Gets name of photo file bar the extension.
     *
     * @return name of photo
     */
    public String getName() {
        return file.getName().substring(0, file.getName().lastIndexOf("."));
    }

    /**
     * Gets caption of a photo.
     *
     * @return String caption of a photo.
     */
    public String getCaption() {
        return caption;
    }

    /**
     * Gets all tags that a Photo has.
     *
     * @return all tags of a photo
     */
    public List<Tag> getTags() {
        return tags;
    }

    /**
     * Gets Calendar object of photo file's last modified date.
     *
     * @return Calendar object of photo's date
     */
    public Calendar getCalendar() {
        zeroOutDate();
        return dateLastModified;
    }

    /**
     * Gets Date object of photo file's last modified date.
     *
     * @return Date object of photo's date
     */
    public Date getDate() {
        return getCalendar().getTime();
    }

    /**
     * String representation, MMM d, yyyy of the photo's date.
     *
     * @return string representation of date
     */
    public String getStringDate() {
        Date date = getCalendar().getTime();
        SimpleDateFormat stringDate = new SimpleDateFormat("MMM d, yyyy");
        return stringDate.format(date);
    }

    /**
     * Gets all static extensions that a photo is allowed to have.
     *
     * @return List of static extension Strings.
     */
    public static List<String> getExtensions() {
        return extensions;
    }

    /**
     * Helper method to get file extension of a file.
     *
     * @param f File
     * @return extension of file
     */
    public static String getFileExtension(File f) {
        String fileName = f.getName();
        int dotIndex = fileName.lastIndexOf('.');
        return (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1).toLowerCase();
    }

    /**
     * Sets the Album of a photo for copying purposes.
     *
     * @param album Album to set for photo
     */
    public void setAlbum(Album album) {
        this.album = album;
    }

    /**
     * Sets the caption of a photo and updates it across all albums.
     *
     * @param caption String caption to set
     */
    public void setCaption(String caption) {
        this.caption = caption;
        album.updatePhotoAcrossAlbums(this);
    }

    /**
     * Adds a new non-duplicate tag to Photo's tags and updates it across all albums.
     *
     * @param t Tag to add
     */
    public void addTag(Tag t) {
        if (!tags.contains(t)) {
            tags.add(t);
            album.updatePhotoAcrossAlbums(this);
        }
    }

    /**
     * Removes a tag from a Photo and updates it across all albums.
     *
     * @param t Tag to remove
     */
    public void removeTag(Tag t) {
        if (tags.remove(t))
            album.updatePhotoAcrossAlbums(this);
    }

    /**
     * Helper function to zero out time for dateLastModified.
     */
    private void zeroOutDate() {
        dateLastModified.set(Calendar.HOUR_OF_DAY, 0);
        dateLastModified.set(Calendar.MINUTE, 0);
        dateLastModified.set(Calendar.SECOND, 0);
        dateLastModified.set(Calendar.MILLISECOND, 0);
    }

    /**
     * String Representation of Photo.
     *
     * @return String representation of Photo
     */
    @Override
    public String toString() {
        return "Photo{" +
                "dateLastModified=" + getStringDate() +
                ", name=" + file.getName() +
                ", tags=" + tags +
                ", caption='" + caption + '\'' +
                '}';
    }

    /**
     * Equality method for photo comparison that compares based on file of photo.
     *
     * @param o any Object
     * @return if Photo == o
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Photo photo = (Photo) o;
        return file.equals(photo.file);
    }

    /**
     * Comparing photos based on date last modified so sorter can sort based on date.
     *
     * @param o the object to be compared.
     * @return date comparison int
     */
    @Override
    public int compareTo(Photo o) {
        return getDate().compareTo(o.getDate());
    }

    /**
     * Hash representation of photo.
     *
     * @return hash of photo
     */
    @Override
    public int hashCode() {
        return Objects.hash(file);
    }
}
