package model;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User that holds Albums and Photos.
 *
 * @author Kenneth Salanga
 */
public class User implements Serializable {
    /**
     * ID when generating serialized object file.
     */
    static final long serialVersionUID = 2L;

    /**
     * Username.
     */
    private final String name;

    /**
     * Albums that the user has.
     */
    private final List<Album> albums;

    /**
     * Available tag types that a user can use to label and search for specific photos with those keys.
     */
    private final List<TagType> tagTypes;

    /**
     * Instantiates a user with a username.
     *
     * @param name username to provide User object
     */
    public User(String name) {
        this.name = name.trim();
        this.albums = new ArrayList<>();
        this.tagTypes = TagType.getDefaultTagTypes();
    }

    /**
     * Create an album and add to the user's list of Albums and writes it to database.
     *
     * @param album Album object that can have a predefined set of Photos
     * @throws IOException
     */
    public void createAlbum(Album album) throws IOException {
        if (!albums.contains(album)) {
            albums.add(album);
            UsersData.writeUser(this);
        }
    }

    /**
     * Create an album and add to the user's list of Albums and writes it to database.
     *
     * @param albumName Album name that will instantiate a new Album with no Photos
     * @throws IOException
     */
    public void createAlbum(String albumName) throws IOException {
        Album album = new Album(albumName, this);
        if (!albums.contains(album)) {
            albums.add(album);
            UsersData.writeUser(this);
        }
    }

    /**
     * Rename a given album to a new name if that new album name doesn't yet exist for User and writes it to database.
     *
     * @param album Album to be renamed
     * @param newName new name to give Album
     * @throws IOException
     */
    public void renameAlbum(Album album, String newName) throws IOException {
        if (!albums.contains(new Album(newName, this))) {
            Album renameAlbum = getAlbum(album);
            renameAlbum.setName(newName);
            UsersData.writeUser(this);
        }

    }

    /**
     * Deletes an existing album from a user's list of albums and writes it to database.
     *
     * @param album Album to be deleted
     * @throws IOException
     */
    public void deleteAlbum(Album album) throws IOException {
        if (albums.contains(album)) {
            albums.remove(album);
            UsersData.writeUser(this);
        }
    }

    /**
     * Gets a set of all Photos a user has.
     *
     * @return set of all photos
     */
    public Set<Photo> getAllPhotos() {
        Set<Photo> photoSet = new HashSet<>();

        for (Album album : this.getAlbums()) {
            photoSet.addAll(album.getPhotos());
        }

        return photoSet;
    }

    /**
     * Adds a tag type to a User's list of tag keys and writes it to database.
     *
     * @param tagType TagType to add
     * @throws IOException
     */
    public void addTagType(TagType tagType) throws IOException {
        if (!tagTypes.contains(tagType)) {
            tagTypes.add(tagType);
            UsersData.writeUser(this);
        }
    }

    /**
     * Deletes a TagType from a User's list of tag keys and writes it to database.
     *
     * @param tagType String key to delete
     * @throws IOException
     */
    public void removeTagType(TagType tagType) throws IOException {
        if (tagTypes.contains(tagType)) {
            tagTypes.remove(tagType);
            UsersData.writeUser(this);
        }
    }

    /**
     * Gets a list of user's tag types (all lower case and trimmed).
     *
     * @return list of user's tag types
     */
    public List<TagType> getTagTypes() {
        return tagTypes;
    }

    /**
     * Gets username.
     *
     * @return username
     */
    public String getName() {
        return name;
    }

    /**
     * Gets a specific album in a User's album list.
     *
     * @param album Album the user wants to retrieve
     * @return Album the user requested
     */
    public Album getAlbum(Album album) {
        int albumIndex = albums.indexOf(album);

        if (albumIndex != -1){
            return albums.get(albums.indexOf(album));
        }

        return null;
    }

    /**
     * Gets all Albums a user has.
     *
     * @return all of user's albums
     */
    public List<Album> getAlbums() {
        return albums;
    }
}
