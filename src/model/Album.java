package model;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 */
public class Album implements Serializable {
    /**
     * ID when generating serialized object file.
     */
    static final long serialVersionUID = 3L;

    /**
     * User album is associated with.
     */
    private final User user;

    /**
     * Name of album.
     */
    private String name;

    /**
     * Photos that an album has.
     */
    private final List<Photo> photos;

    /**
     * Instantiates an album with name and its respective user.
     *
     * @param name name of album
     * @param user user album is associated with
     */
    public Album(String name, User user) {
        this.name = name.trim();
        this.user = user;
        this.photos = new ArrayList<>();
    }

    /**
     * Moves Photo from this album to moveAlbum and deletes photo on this album.
     *
     * @param photo Photo to move
     * @param moveAlbum Album where photo is going to
     * @throws IOException
     */
    public void movePhoto(Photo photo, Album moveAlbum) throws IOException {
        if (user.getAlbums().contains(moveAlbum)) {
            user.getAlbum(moveAlbum).addPhoto(photo);
            removePhoto(photo);
        }
    }

    /**
     * Copies Photo from this album to copyAlbum and updates the photo's associated album by creating a new photo.
     *
     * @param photo Photo to copy
     * @param copyAlbum Album where photo is going to
     * @throws IOException
     */
    public void copyPhoto(Photo photo, Album copyAlbum) throws IOException {
        if (user.getAlbums().contains(copyAlbum)) {
            Photo copyPhoto = new Photo(photo);
            copyPhoto.setAlbum(copyAlbum);
            user.getAlbum(copyAlbum).addPhoto(copyPhoto);
        }
    }

    /**
     * Album will look at other Albums of user to update a Photo's metadata across all albums that have the same photo and updates the database.
     *
     * @param photo Photo to update
     */
    protected void updatePhotoAcrossAlbums(Photo photo) {
        for (Album album : user.getAlbums()) {
            int photosIndex = album.getPhotos().indexOf(photo);
            if (photosIndex != -1) {
                album.getPhotos().set(photosIndex, photo);
            }
        }

        try {
            UsersData.writeUser(user);
        } catch (IOException e) {
            System.out.println("IO Exception in updating photo:");
            e.printStackTrace();
        }
    }

    /**
     * Gets name of Album.
     *
     * @return name of album
     */
    public String getName() {
        return name;
    }

    /**
     * Gets a specific photo on an album.
     *
     * @param photo Photo to get
     * @return Requested photo or null if photo isn't found
     */
    public Photo getPhoto(Photo photo) {
        int photoIndex = photos.indexOf(photo);

        if (photoIndex != -1){
            return photos.get(photos.indexOf(photo));
        }

        return null;
    }

    /**
     * Gets a list of all photos of an album.
     *
     * @return list of all photos
     */
    public List<Photo> getPhotos() {
        return photos;
    }

    /**
     * Sets name of album.
     *
     * @param name name to set album
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Adds a photo to the Album. If the photo already exists on the album, create a new photo with that existing photo's metadata.
     * If the photo isn't a duplicate, add the photo to the album's list of photos and write changes to database of user.
     *
     * @param photo photo to add
     * @throws IOException
     */
    public void addPhoto(Photo photo) throws IOException {
        for (Album album : user.getAlbums()) {
            List<Photo> albumPhotos = album.getPhotos();
            if (albumPhotos.contains(photo)) {
                photo = new Photo(albumPhotos.get(albumPhotos.indexOf(photo)));
                photo.setAlbum(this);
                break;
            }
        }

        if (!photos.contains(photo)) {
            photos.add(photo);
            UsersData.writeUser(user);
        }
    }

    /**
     * Removes a photo that exists on an album's photo list and if the photo exists, write to the database of user.
     *
     * @param photo photo to remove
     * @throws IOException
     */
    public void removePhoto(Photo photo) throws IOException {
        if (photos.contains(photo)) {
            photos.remove(photo);
            UsersData.writeUser(user);
        }
    }

    /**
     * Equality based on case-insensitive name.
     *
     * @param o any object
     * @return if album equals object or not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Album album = (Album) o;
        return name.toLowerCase().equals(album.getName().toLowerCase());
    }

    /**
     * String representation of Album that shows name, number of photos, and date range
     *
     * @return string representation of album
     */
    @Override
    public String toString() {
        Collections.sort(photos);
        int numPhotos = photos.size();

        String albumString = "";

        albumString += "Name: " + getName() + "\n";
        albumString += "Number of photos: " + numPhotos + "\n";

        if (numPhotos > 0) {
            albumString += "Date Range: " + photos.get(0).getStringDate() + " - " + photos.get(numPhotos - 1).getStringDate();
        }
        return  albumString;
    }
}
