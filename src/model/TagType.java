package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Tag Types that a user has that act as the tag "keys" of a given tag
 *
 * @author Kenneth Salanga
 */
public class TagType implements Comparable<TagType>, Serializable {
    /**
     * ID when generating serialized object file.
     */
    static final long serialVersionUID = 6L;

    /**
     * Name of tagType.
     */
    private final String name;

    /**
     * Value of tag type (Multiple or Single).
     */
    private final Value value;

    /**
     * Default tag types that all users will start with.
     */
    private static final List<TagType> defaultTags = new ArrayList<>(Arrays.asList(new TagType("location", Value.SINGLE), new TagType("name", Value.MULTIPLE)));

    /**
     * Instantiate a tag type that has a name and Value (Multiple or Single).
     *
     * @param name String name of tag type
     * @param value Value (multiple or single) of tag type
     */
    public TagType(String name, Value value) {
        this.name = name.trim();
        this.value = value;
    }

    /**
     * Gets name of tag type.
     *
     * @return String name of tag type
     */
    public String getName() {
        return name;
    }

    /**
     * Gets Value of tag type.
     *
     * @return Multiple or Single Value
     */
    public Value getValue() {
        return value;
    }

    /**
     * Gets default tag types that all starting users will have.
     *
     * @return list of default tags
     */
    public static List<TagType> getDefaultTagTypes() {
        return defaultTags;
    }

    /**
     * String representation of TagType.
     *
     * @return String name
     */
    @Override
    public String toString() {
        if (value == Value.SINGLE) {
            return name + " (Single)";
        }
        return name + " (Multiple)";
    }

    /**
     * Tag case insensitive comparison.
     *
     * @param t TagType to be compared
     * @return if this tagType is == to tagType t
     */
    @Override
    public int compareTo(TagType t) {
        return this.name.toLowerCase().compareTo(t.getName().toLowerCase());
    }

    /**
     * Equality method for comparing tag types.
     *
     * @param o Object to be compared
     * @return if this tagType is == o
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TagType tagType = (TagType) o;
        return compareTo(tagType) == 0;
    }
}
