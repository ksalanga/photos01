package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Searcher function for a User's set of photos that searches based on tag and dates.
 *
 * @author Kenneth Salanga
 */
public class PhotoSearcher {
    /**
     * User that the searcher will search for.
     */
    private final User user;

    /**
     * Instantiates a photo searcher for a user.
     *
     * @param user user that the searcher will search for
     */
    public PhotoSearcher(User user) {
        this.user = user;
    }

    /**
     * Search for photos that are equal to this date.
     *
     * @param date Date to search Photos for
     * @return Photos that have this date
     */
    public List<Photo> search(Date date) {
        return user.getAllPhotos().stream()
                .filter(photo -> photo.getDate().equals(date))
                .collect(Collectors.toList());
    }

    /**
     * Search for photos between a range of dates (inclusive).
     *
     * @param dates dates[0] is the earliest date and dates[1] is the latest
     * @return Photos that fall in this date range
     */
    public List<Photo> search(Date[] dates) {
//        photo ->
//                (photo.getDate().after(dates[0]) && photo.getDate().before(dates[1])
//                        || (photo.getDate().equals(dates[0]) || photo.getDate().equals(dates[1])))
        return user.getAllPhotos().stream()
                .filter(photo -> !dates[0].after(photo.getDate()) && !dates[1].before(photo.getDate()))
                .collect(Collectors.toList());
    }

    /**
     * Search for photo based on a single tag.
     *
     * @param tag Tag to search photos for
     * @return Photos that have this tag
     */
    public List<Photo> search(Tag tag) {
        return user.getAllPhotos().stream()
                .filter(photo -> photo.getTags().contains(tag))
                .collect(Collectors.toList());
    }

    /**
     * Conjunction search for two tags.
     *
     * @param tags Two tags to search photos for
     * @return Photos that have both tags
     */
    public List<Photo> searchAnd(Tag[] tags) {
        Predicate<Photo> tag1Matches = photo -> photo.getTags().contains(tags[0]);
        Predicate<Photo> tag2Matches = photo -> photo.getTags().contains(tags[1]);

        return user.getAllPhotos().stream()
                .filter(tag1Matches.and(tag2Matches))
                .collect(Collectors.toList());
    }

    /**
     * Disjunction search for two tags.
     *
     * @param tags Two tags to search photos for
     * @return Photos that have one tag or the other
     */
    public List<Photo> searchOr(Tag[] tags) {
        Predicate<Photo> tag1Matches = photo -> photo.getTags().contains(tags[0]);
        Predicate<Photo> tag2Matches = photo -> photo.getTags().contains(tags[1]);

        return user.getAllPhotos().stream()
                .filter(tag1Matches.or(tag2Matches))
                .collect(Collectors.toList());
    }

    /**
     * Checks if a string is a correct date format mm/dd/yyyy-mm/dd/yyyy or mm/dd/yyyy.
     *
     * @param date String representation of date
     * @return if string is correct format
     */
    public static boolean correctDateFormat(String date) {
        return date.matches("^\\d{1,2}/\\d{1,2}/\\d{4}-\\d{1,2}/\\d{1,2}/\\d{4}$") || date.matches("^\\d{1,2}/\\d{1,2}/\\d{4}$");
    }

    /**
     * Checks if two dates are in the correct order where the earliest date is before the latest date.
     *
     * @param dates dates[0] is the earliest date and dates[1] is the latest date
     * @return if the dates are in the correct order
     */
    public static boolean datesCorrectOrder(Date[] dates) {
        return dates[0].getTime() <= dates[1].getTime();
    }

    /**
     * Converts string date representation assuming correctDateFormat is true, to date object(s).
     *
     * @param date String date representation
     * @return Date array of size 2, if single date(mm/dd/yyyy), date is in the first element, and if ranged, returns two dates
     * @throws ParseException
     */
    public static Date[] stringToDates(String date) throws ParseException {
        Date dates[] = new Date[2];

        if (date.contains("-")) {
            String dateStrings[] = date.split("-");
            String date1String = dateStrings[0];
            String date2String = dateStrings[1];

            dates[0] = new SimpleDateFormat("MM/dd/yyyy").parse(date1String);
            dates[1] = new SimpleDateFormat("MM/dd/yyyy").parse(date2String);
        } else {
            dates[0] = new SimpleDateFormat("MM/dd/yyyy").parse(date);
            dates[1] = null;
        }

        return dates;
    }

    /**
     * Checks if String tag is correct Tag Format: key=value or key1=value1 AND key2=value2 or key1=value1 OR key2=value2.
     *
     * @param tags String representation of tag search
     * @return if string tag is correct tag format
     */
    public static boolean correctTagFormat(String tags) {
        return tags.matches("^[a-zA-Z0-9]*=[^ ][a-zA-Z0-9 ]* AND [a-zA-Z0-9]*=[^ ][a-zA-Z0-9 ]*$")
                || tags.matches("^[a-zA-Z0-9]*=[^ ][a-zA-Z0-9 ]* OR [a-zA-Z0-9]*=[^ ][a-zA-Z0-9 ]*$")
                || tags.matches("^[a-zA-Z0-9]*=[^ ][a-zA-Z0-9 ]*$");
    }

    /**
     * Converts string tag representation assuming correctTagFormat is true, to tag object(s).
     *
     * @param tagString String tag representation
     * @return Tag array of size 2, if single tag, tag is in the first element, and if ranged, returns two tags
     */
    public static Tag[] stringToTags(String tagString) {
        Tag[] tags = new Tag[2];
        if (tagString.contains(" AND ") || tagString.contains(" OR ")) {
            String[] tagStrings = tagString.contains(" AND ") ? tagString.split(" AND ", 2) : tagString.split(" OR ", 2);

            String key1 = tagStrings[0].split("=")[0];
            String value1 = tagStrings[0].split("=")[1];
            tags[0] = new Tag(key1, value1);

            String key2 = tagStrings[1].split("=")[0];
            String value2 = tagStrings[1].split("=")[1];
            tags[1] = new Tag(key2, value2);
        } else {
            String key = tagString.split("=")[0].trim();
            String value = tagString.split("=")[1].trim();

            tags[0] = new Tag(key, value);
            tags[1] = null;
        }
        return tags;
    }

}
