package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Tag with Key, Value pair that can be used to describe and search for Photos
 *
 * @author Kenneth Salanga
 */
public class Tag implements Comparable<Tag>, Serializable {
    /**
     * ID when generating serialized object file.
     */
    static final long serialVersionUID = 5L;

    /**
     * Key of a tag.
     */
    private final String key;

    /**
     * Value of a tag.
     */
    private final String value;

    /**
     * Instantiates a tag with key, value pair.
     *
     * @param key String key
     * @param value String value
     */
    public Tag(String key, String value) {
        this.key = key.trim();
        this.value = value.trim();
    }

    /**
     * Gets key of tag
     *
     * @return String key of tag
     */
    public String getKey() {
        return key;
    }

    /**
     * Gets value of tag
     *
     * @return String value of tag
     */
    public String getValue() {
        return value;
    }

    /**
     * String representation of tag.
     *
     * @return key=value String
     */
    @Override
    public String toString() {
        return key + "=" + value;
    }

    /**
     * Tag case insensitive comparison.
     *
     * @param t Tag to be compared
     * @return if this tag is == to tag t
     */
    @Override
    public int compareTo(Tag t) {
        return this.toString().toLowerCase().compareTo(t.toString().toLowerCase());
    }

    /**
     * Equality method for comparing tags.
     *
     * @param o Object to be compared
     * @return if this tag is == o
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return compareTo(tag) == 0;
    }
}
