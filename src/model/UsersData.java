package model;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Database for users that writes and reads files using object serialization.
 *
 * @author Kenneth Salanga
 */
public class UsersData implements Serializable {
	/**
	 * ID when generating serialized object file.
	 */
	static final long serialVersionUID = 1L;

	/**
	 * Starting User Directory.
	 */
	private static final String userDir = "data";

	/**
	 * Extension used for storing User files.
	 */
	private static final String extension = ".dat";

	/**
	 * Writes a User object to an ObjectOutput stream to "username.dat" in the User Directory.
	 * Flush and close will overwrite the file on each new write.
	 *
	 * @param user User object that will write its current state to "username.dat"
	 * @throws IOException
	 */
	protected static void writeUser(User user) throws IOException {
		ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream(userDir + File.separator + user.getName() + extension));
		oos.writeObject(user);
		oos.flush();
		oos.close();
	}

	/**
	 * Reads a User Object to an ObjectInput stream from "username.dat" in the User Directory.
	 * If the user does not yet exist in the directory, create/write a new user.
	 * Else, deserialize the file to give back a User object.
	 *
	 * @param user User object that will read contents from the serialized "username.dat"
	 * @return Deserialized User object with its current state that was in the file
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static User getUser(User user) throws IOException, ClassNotFoundException {
		if (!new File(userDir + File.separator + user.getName() + extension).exists()) {
			writeUser(user);
		}

		ObjectInputStream ois = new ObjectInputStream(
				new FileInputStream(userDir + File.separator + user.getName() + extension));
		User newUser = (User)ois.readObject();
		ois.close();
		return newUser;
	}

	/**
	 * Deletes the "username.dat" file.
	 *
	 * @param name Username that the client wants to delete
	 * @throws IOException
	 */
	public static void deleteUser(String name) throws IOException {
		Path of = Path.of("./data/" + name + extension);
		if (Files.exists(of)) {
			Files.walk(of).sorted(Comparator.reverseOrder())
					.map(Path::toFile)
					.forEach(File::delete);
		}
	}

	/**
	 * Gets a list of all usernames bar the extension.
	 *
	 * @return list of usernames in the User Directory
	 */
	public static List<String> getUserList() {
		File usersDirectory = new File(userDir);

		List<String> usersListExtensions = new ArrayList<>(Arrays.asList(Objects.requireNonNull(usersDirectory.list((dir, name) -> !name.equals("stock photos")))));

		List<String> usersList = new ArrayList<>();

		for (String s : usersListExtensions) {
			usersList.add(s.substring(0, s.lastIndexOf(".")));
		}

		return usersList;
	}

	/**
	 * Initializes a stock user with a predefined state (albums, photos).
	 *
	 * @throws IOException
	 */
	public static void initializeStockUser() throws IOException {
		User stockUser = new User("stock");

		Album stockAlbum = new Album("stock", stockUser);
		stockUser.createAlbum(stockAlbum.getName());

		stockAlbum = stockUser.getAlbum(stockAlbum);

		File stockPhotosDirectory = new File(userDir + File.separator + "stock photos");
		File[] stockPhotos = stockPhotosDirectory.listFiles();

		assert stockPhotos != null;
		for (File f : stockPhotos) {
			stockAlbum.addPhoto(new Photo(f, stockAlbum));
		}

		writeUser(stockUser);
	}

}
