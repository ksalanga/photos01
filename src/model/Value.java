package model;

/**
 * Enum that says whether a Tag is of Value Multiple or Single
 */
public enum Value {
    SINGLE,
    MULTIPLE
}
